

### Use case: loading slowly changing dimensions

campiagn DB table (dataframe):

#### Before:
`id,uuid,name, code, rate, audit_start_date`
(empty)

#### Process data:
(read and process the data in ./data/campaigns folder)

#### After:
`
id,uuid,name, code, rate, effective_start_date
1,A,campiagnA,A1,1.01,2017-12-24T01:00:00Z
2,A,campiagnA,A2,1.01,2017-12-24T01:00:01Z
3,A,campiagnA-renamed,A3,1.02,2017-12-24T01:01:00Z
4,B,campiagnB,B1,2.01,2017-12-24T01:00:01Z
5,B,campiagnB,B1,2.02,2017-12-24T01:00:02Z
6,B,campiagnB,B1,2.03,2017-12-24T01:00:04Z
7,C,campiagnC,C1,1.01,2017-12-24T02:00:00Z
`


### Notes:
* The script should be able to process multiple events per specific campaign 
    * campaign A exists and the script processes 2 or more update events in a single execution
* Columns:
    * id is incremental
    * uuid is a unique identifier; it never changes; it's the same for a single campaign (all versions)
    * name - slowly changing dimension type1 (not versioned; overwritten)
    * code - slowly changing dimension type2 (versioned)
    * reate - slowly changing dimension type2 (versioned)
    * effective_start_date - start date of the new campaign version
* This is the hive/reload approach but I don't want to do it this way; it feels too complicated: https://www.youtube.com/watch?v=IArhNttLdmM&t=2635s
* These may be useful: 
    * https://gist.github.com/rampage644/cc4659edd11d9a288c1b
    * https://github.com/cartershanklin/hive-scd-examples - best example
        * https://hortonworks.com/blog/update-hive-tables-easy-way-2/ - description


### Questions:
* How to do deduplication?
* How to load dimenstions?
* Replace the whole dataset?
* Store on s3?
* Load before facts?
